const fs = require('fs');
const path = require('path');

const NUMBER_MAP = {
  one: 1, two: 2, three: 3, four: 4, five: 5,
  six: 6, seven: 7, eight: 8, nine: 9, zero: 0,
};

const sum = (lines = [], matchWords = false) =>
  lines.reduce((acc, line) => {
    const matches = line.match(
      matchWords ? new RegExp(`(?:\\d|${Object.keys(NUMBER_MAP).join('|')})`, 'g') : /\d/g,
    );
    const normalize = (s) => isNaN(Number(s)) ? NUMBER_MAP[s] ?? 0 : parseInt(s, 10);
    acc += matches ? normalize(matches[0]) * 10 + normalize(matches[matches.length - 1]) : 0;
    return acc;
  }, 0);

if (process.argv.length < 3) {
  console.log(
    `Usage: node ${path.basename(
      process.argv[1],
    )} <input_file> [match_words=false]`,
  );
  process.exit(1);
}

const filename = process.argv[2];
const matchWords = process.argv[3] === 'true' || process.argv[3] === '1';
const lines = fs.readFileSync(filename, 'utf-8').split('\n');
const output = sum(lines, matchWords);
console.log(`Output: ${output}`);
