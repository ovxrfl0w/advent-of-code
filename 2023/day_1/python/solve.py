#!/usr/bin/env python3

import re
import sys

NUMBER_MAP = {
    "one": 1, "two": 2, "three": 3, "four": 4, "five": 5,
    "six": 6, "seven": 7, "eight": 8, "nine": 9, "zero": 0
}

def normalize(s: str) -> int:
    return int(s) if s.isdigit() else NUMBER_MAP.get(s, 0)

def calibrate(line: str, match_words: bool) -> int:
    matches = re.findall(
        rf'(?=(\d|{"|".join(NUMBER_MAP)}))' if match_words else r'\d', line
    )
    return normalize(matches[0]) * 10 + normalize(matches[-1]) if matches else 0

if len(sys.argv) < 2:
    print(f"Usage: python3 {sys.argv[0]} <input_file> [match_words=False]")
    sys.exit(1)
else:
    with open(sys.argv[1], "r") as file:
        lines = file.readlines()
        match_words = (len(sys.argv) > 2) and (sys.argv[2] in ["true", "1"])
        print(f"Output: {sum(calibrate(line, match_words) for line in lines)}")
