#!/usr/bin/env python3

import re
import sys

def check_game(game: str, check_minimum: bool = False) -> int:
    if check_minimum:
        matches = re.findall(r'(\d+) (\w+)', game)
        cube_map = {"red": 0, "green": 0, "blue": 0}
        for count, color in matches:
            cube_map[color] = max(cube_map[color], int(count))
        return cube_map["red"] * cube_map["green"] * cube_map["blue"]
    else:
        if re.search(r'(1[3-9]|[2-9][0-9]+) red|(1[4-9]|[3-9][0-9]+) green|(1[5-9]|[4-9][0-9]+) blue', game):
            return 0
        return int(game.split(':')[0].split(" ")[1])

if len(sys.argv) < 2:
    print(f"Usage: python3 {sys.argv[0]} <input_file> [check_minimum=False]")
    sys.exit(1)
else:
    with open(sys.argv[1], "r") as file:
        lines = file.readlines()
        check_minimum = (len(sys.argv) > 2) and (sys.argv[2] in ["true", "1"])
        print(f"Output: {sum(check_game(line, check_minimum) for line in lines)}")
